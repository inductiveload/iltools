

function guessPagenumber( fn ) {

	let m = fn.match(/(?:[Pp]|[Pp]age|[Pp]g)[ _]?(([0-9]+|[ivxlc]+)(?:-([0-9]+))?)\b/);

	return m ? m[1] : null;
}

module.exports = {
	guessPagenumber
};
