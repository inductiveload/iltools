
const specialTypes = {
    "illustration": {
        "desc": "Illustration from {src}",
        "cats": ["Book illustrations"],
        "title_comp": "illustration",
        "name": "Illustration"
    },
    "photograph": {
        "desc": "Photograph from {src}",
        "cats": ["Photographic illustrations"],
        "title_comp": "photograph",
        "name": "Photograph"
    },
    "headpiece": {
        "desc": "Headpiece from {src}",
        "cats": ["Headpieces (book illustration)"],
        "title_comp": "headpiece",
        "name": "Headpiece"
    },
    "endpiece": {
        "desc": "Endpiece from {src}",
        "cats": ["Endpieces (book illustration)"],
        "title_comp": "endpiece",
        "name": "Endpiece"
    },
    "nameplate": {
        "desc": "Nameplate from {src}",
        "cats": ["Newspaper nameplates"],
        "title_comp": "nameplate",
        "name": "Nameplate (newspaper or periodical)"
    },
    "divider": {
        "desc": "Text divider from {src}",
        "cats": ["Text dividers"],
        "title_comp": "divider",
        "name": "Text divider or rule"
    },
    "frontispiece": {
        "desc": "Frontispiece from {src}",
        "cats": ["Frontispieces"],
        "title_comp": "frontispiece",
        "name": "Frontispiece"
    },
    "fleuron": {
        "desc": "Fleuron from {src}",
        "cats": ["Fleurons"],
        "title_comp": "fleuron",
        "name": "Fleuron"
    },
    "title": {
        "desc": "Title from {src}",
        "cats": ["Titles (text)"],
        "title_comp": "title",
        "name": "Title or heading"
    },
    "initial": {
        "desc": "Initial {initial} from {src}",
        "cats": ["{initial} as an initial"],
        "title_comp": "initial {initial}",
        "name": "Initial"
    }
};

function isValidType( cand ) {
    return specialTypes[cand] !== undefined;
}

function getTypeInfo(type) {
	return specialTypes[type];
}

module.exports = {
	getTypeInfo,
    isValidType,
    specialTypes
};
