
function scoreLink( link ) {

	if ( link.startsWith( '//' ) ) {
		link = 'https://' + link;
	}

	const url = new URL( link );

	if ( url.hostname.includes("hathitrust") ||
			url.hostname.endsWith("archive.org") ) {
		return 100;
	}

	return -1;
}

/*
 * Returns a promise resolving with the URL of a source for a file
 */
function guessSource( wiki, title ) {

	const params = {
		action: 'query',
		format: 'json',
		formatversion: 2,
		origin: '*',
		prop: 'revisions|extlinks|iwlinks',
		rvslots: 'main',
		rvprop: 'content',
		titles: title,
		rvlimit: 1
	};

	return fetch(wiki + '/w/api.php?' + new URLSearchParams(params))
		.then( res => res.json() )
		.then( data => {

			if ( data.query.pages[0].iwlinks ) {
				let iws = data.query.pages[0].iwlinks.filter( iw => {
					return [ 'iarchive' ].indexOf( iw.prefix ) !== -1;
				})

				if ( iws.length ) {
					switch( iws[0].prefix ) {
					case 'iarchive':
						return `https://archive.org/details/${iws[0].title}`;
					}
				}
			}

			if ( data.query.pages[0].extlinks ) {

				let scored = data.query.pages[0].extlinks.map( l => {
					return {
						score: scoreLink( l.url ),
						url: l.url
					};
				} ).filter( l => l.score > 0 );

				scored.sort( l => -l.score );

				if ( scored.length ) {
					return scored[0].url;
				}
			}

		} );
}

// const { mwn } = require('mwn');

function getRcForOauthCidUrl ( wiki, cid ) {
	const params = {
		tagfilter: 'OAuth CID: ' + cid,
	};

	return wiki + '/wiki/Special:RecentChanges?' + new URLSearchParams(params);
}

function getRecentPrefixEdits( wiki, prefix, namespace ) {

	let params = {
		format: 'json',
		formatversion: 2,
		origin: '*',
		action: 'query',
    list: 'allpages',
    aplimit: 15,
    apfrom: prefix,
    apprefix: prefix,
	}

  if (namespace ) {
    params.apnamespace = namespace;
  }

	return fetch(wiki + '/w/api.php?' + new URLSearchParams(params))
		.then( res => res.json() )
		.then( (data) => {
			return data.query.allpages;
		} );
}

function uniquePages(a) {
	var seen = {};
	return a.filter(function(item) {
		return Object.prototype.hasOwnProperty.call(seen, item.pageid) ? false : (seen[item.pageid] = true);
	});
}

function getRecentUserEdits( wiki, user, namespace ) {

	let params = {
		format: 'json',
		formatversion: 2,
		origin: '*',
		action: "query",
		list: "usercontribs",
		uclimit: 25,
		ucuser: user,
	};
	if (namespace) {
		params.ucnamespace = namespace;
	}

	return fetch(wiki + '/w/api.php?' + new URLSearchParams(params))
		.then( res => res.json() )
		.then( (data) => {
			const contribs = data.query.usercontribs;
			return uniquePages(contribs);
		} );
}

function pageExists( wiki, page ) {
	const params = {
		format: 'json',
		formatversion: 2,
		origin: '*',
		action: "query",
		titles: page
	};

	return fetch(wiki + '/w/api.php?' + new URLSearchParams(params))
		.then( res => res.json() )
		.then( (data) => {
			const missing = data.query.pages[0].missing;
			const invalid = data.query.pages[0].invalid;
			return !(invalid || missing);
		} );
}

function getWikidataSuggestions(s) {
	const params = {
		action: 'wbsearchentities',
		format: 'json',
		origin: '*',
		search: s,
		language: 'en'
	};

	const wd = "https://www.wikidata.org";

	return fetch(wd + '/w/api.php?' + new URLSearchParams(params))
		.then( res => res.json() )
		.then( d => d.search );
}

function getWikitext( wiki, title ) {

	const params = {
		action: 'query',
		format: 'json',
		formatversion: 2,
		origin: '*',
		prop: 'revisions',
		rvslots: 'main',
		rvprop: 'content',
		titles: title,
		rvlimit: 1
	};

	return fetch(wiki + '/w/api.php?' + new URLSearchParams(params))
		.then( res => res.json() )
		.then( data => {
			if ( !data.query ) {
				return null;
			}
			return data.query.pages[0].revisions[0].slots.main.content;
		} );
}


const NS = {
	FILE: 6,
	INDEX: 106, /* only on enWS ;-( */
}

module.exports = {
	guessSource,
	getRcForOauthCidUrl,
	getRecentPrefixEdits,
	getRecentUserEdits,
	pageExists,
	getWikidataSuggestions,
	getWikitext,
	NS
};
