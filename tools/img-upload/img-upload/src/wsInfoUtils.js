

function getDoNotMoveToCommons( subdomain, expiry ) {
	if ( subdomain === 'en' ) {

		if ( expiry ) {
			return `{{do not move to Commons|expiry=${expiry}}}`
		}
		return '{{do not move to Commons}}';
	}

	return null;
}


module.exports = {
	getDoNotMoveToCommons
};