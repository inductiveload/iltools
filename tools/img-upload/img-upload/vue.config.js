//vue.config.js
module.exports = {
	chainWebpack: config => {
		config
			.plugin('html')
			.tap(args => {
				args[0].title = "Wikisource Image Uploader";
				return args;
			})
	},

  configureWebpack:{
		optimization: {
			splitChunks: {
				minSize: 10000,
				maxSize: 250000,
			}
		}
  },

	lintOnSave: false
}