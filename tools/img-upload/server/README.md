

Create `.flaskenv`:

```
FLASK_APP=app.py
FLASK_RUN_HOST=localhost
FLASK_RUN_PORT=5657
```

Create `.env` from `.env.sample`

### INstall environment

```
python3 -m venv venv
source venv/bin/activate
pip install -f requirements.txt
```

### Run the server

```
flask run
```

## On Toolforge

Create `.env` as before but use your real OAuth credentials.

### First run

```
ln -s ~/iltools/tools/img-upload/server ~/www/python/src
webservice --backend=kubernetes python3.7 shell
cd www/python
python3 -m venv venv
source venv/bin/activate
pip install --upgrade pip setuptools
pip install -r src/requirements.txt
```

Run the server (exit the shell if needed)

```
webservice --backend=kubernetes python3.7 start
```

### Upgrade

```
cd ~/iltools
git pull
webservice --backend=kubernetes python3.7 restart
```

