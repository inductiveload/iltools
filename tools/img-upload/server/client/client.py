"""
Simple blueprint to serve the built VueJS app
"""

from flask import Blueprint, render_template
from flask_cors import cross_origin

client_bp = Blueprint('client_bp', __name__,
                      template_folder='../../img-upload/dist',
                      static_url_path='/',
                      static_folder='../../img-upload/dist',
                      )

# Apparently (i.e. experimentally), '/' goes last so when we
# return from the callback, we go there.
@client_bp.route("/about")
@client_bp.route("/")
@cross_origin()
def index():
    return render_template('index.html')
