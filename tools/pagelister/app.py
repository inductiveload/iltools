#!/usr/bin/env python3

import flask

import os
import dotenv

# static utils
import il_utils.sources.ht_api as HTAPI

# blueprints
from pagelist import pagelist
from img_links import img_links

app = flask.Flask(__name__)


def setup_ht_api():

    client_key = os.environ.get("ILTOOLS_HATHI_KEY")
    client_secret = os.environ.get("ILTOOLS_HATHI_SECRET")
    return HTAPI.DataAPI(client_key, client_secret)

dotenv.load_dotenv()

@app.route('/favicon.ico')
def favicon():
    return flask.send_from_directory(
        os.path.join(app.root_path, 'static'),
        'favicon.ico', mimetype='image/vnd.microsoft.icon')

@app.route('/')
@app.route('/index')
def index():
    return flask.render_template('index.html', title='Scan source utilities')

app.register_blueprint(pagelist.pagelist)
app.register_blueprint(img_links.bp)


app.config['HT_DAPI'] = setup_ht_api()

if __name__ == '__main__':
    app.run()
