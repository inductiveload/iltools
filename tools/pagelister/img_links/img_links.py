from flask import Blueprint, current_app, request, jsonify
import flask
from werkzeug.wrappers import response
import time

import il_utils.sources.ia_source as IAS
import il_utils.sources.ht_source as HTS

import requests

from . import links

bp = Blueprint('img_links', __name__,
               template_folder='templates',
               url_prefix='/img_links')


def get_page_with_links(name):

    params = {
        "action": "query",
        "format": "json",
        "formatversion": 2,
        "prop": "iwlinks|extlinks",
        "ellimit": 200,
        "iwlimit": 200,
        "titles": "File:" + name
    }

    api_url = 'https://commons.wikimedia.org/w/api.php'

    r = requests.get(api_url, params=params)
    r.raise_for_status()

    return r.json()['query']['pages'][0]

@bp.route('/')
def index():
    return flask.render_template('img_links.html', title='Image links')


@bp.route('/v1/links', methods=['GET'])
def links_handler():

    start_perf = time.perf_counter()
    start_process = time.process_time()
    errors = []
    source = None

    if 'file' not in request.args and 'url' not in request.args:
        errors.append({'msg': 'No file or url field'})

    if 'file' in request.args and 'url' in request.args:
        errors.append({'msg': 'Cannot have both file and url field'})

    try:
        page = int(request.args['page'])
    except KeyError:
        page = None
    except ValueError:
        errors.append({'msg': f'Invalid page number: {request.args["page"]}'})

    if not errors:

        if 'file' in request.args:
            filepage = get_page_with_links(request.args['file'])

            if not 'missing' in filepage:
                found_links = links.get_links(filepage['iwlinks'], filepage['extlinks'], page)
            else:
                errors.append({'msg': 'File not found'})
        elif 'url' in request.args:
            url = request.args["url"]
            found_links = links.get_links_from_url(url, page)

    if source is not None:
        pass

    end_time = time.perf_counter()

    meta = {
        'perf_time':  1000 * (time.perf_counter() - start_perf),
        'process_time':  1000 * (time.process_time() - start_process)
    }

    if errors:
        response = jsonify({
            "errors": errors,
            "meta": meta
        })
        code = 500
    else:
        response = {
            'links': found_links
        }
        if 'file' in request.args:
            response['file'] = request.args['file']
        if 'url' in request.args:
            response['url'] = request.args['url']

        response['meta'] = meta

        response = jsonify(response)
        code = 200

    response.headers.add('Access-Control-Allow-Origin', '*')
    response.headers.add('X-Robots-Tag', 'noindex')

    return response, code