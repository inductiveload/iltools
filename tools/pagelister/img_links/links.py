from functools import lru_cache
import re
import urllib.parse
import requests
import logging
import traceback
import lxml.html
import lxml.etree

from . import mets


class LinkFinderFactory():

    linkfinder_ctor = None

    def key(self):
        """
        Get the key to identify this factory
        """
        return self.__class__.__name__

    def id_from_url(self, url):
        return None

    def create_from_url(self, url):
        id = self.id_from_url(url)
        if id:
            return self.create_from_id(id)
        return None

    def id_from_url(self, url):
        raise NotImplementedError('LinkFinderFactory have to define a URL to ID mapping')

    def create_finder(self, id):
        raise NotImplementedError

    def create_from_id(self, id):
        if id:
            return self.create_finder(id)

class LinkFinder():

    icon = None
    name = None


    def __init__(self, id) -> None:
        self.id = id

    def get_catalog_url(self):
        """
        Get the canonical catalog entry URL for this link
        """
        return None

    def get_icon(self):
        return self.icon

    def get_name(self):
        return self.name

    def get_links_internal(self, page):
        url = self.get_catalog_url()

        if url:
            link = {
                'type': 'catalog',
                'url': url,
                'title': f'Entry at {self.name}'
            }
            return [link]

        return []

    def get_links(self, page):
        links = self.get_links_internal(page)

        icon = self.get_icon()
        for l in links:
            if 'icon' not in l and icon:
                l['icon'] = icon

        return links

    def make_jpg_link(self, url, highres):
        return {
            'type': 'image',
            'format': 'image/jpeg',
            'highres': highres,
            'url': url,
            'title': f'High-res JPEG at {self.get_name()}'
        }

    def make_tiff_link(self, url):
        """
        MAke a TIFF link - not high res since can't be usefully
        previewed in most JS
        """
        return {
            'type': 'image',
            'format': 'image/tiff',
            'url': url,
            'title': f'High-res TIFF at {self.get_name()}'
        }

class IaLinkFactory(LinkFinderFactory):

    def id_from_url(self, url):
        if url.hostname == 'archive.org':
            m = re.search(r'/details/([^/]+)', url.path )
            if m:
                return m.group(1)
        return None

    def create_from_id(self, id):
        return IaLinkFinder(id)

class IaLinkFinder(LinkFinder):

    icon = 'https://upload.wikimedia.org/wikipedia/commons/thumb/0/05/Archive_org_favicon.png/16px-Archive_org_favicon.png'
    name = 'Internet Archive'

    def get_catalog_url(self):
        return f'https://archive.org/details/{self.id}'

    def get_links_internal(self, page):
        links = super().get_links_internal(page)

        if page is not None:
            jpg_n_index = page - 1
            iiif_index = page

            links.append(self.make_jpg_link(
                f'https://archive.org/download/{self.id}/page/n{jpg_n_index}.jpg',
                highres=True
            ))

            links.append({
                'type': 'iiif',
                'title': 'IIIF image info',
                'url': f'https://iiif.archivelab.org/iiif/{self.id}${iiif_index}/info.json'
            })

        return links

class GoogleBooksFactory(LinkFinderFactory):

    def id_from_url(self, url):
        id = None
        if url.hostname.startswith( 'books.google.' ):
            try:
                id = urllib.parse.parse_qs(url.query)['id'][0]
            except KeyError:
                pass

        # this is a "new" google link
        elif re.match(r'(?:www\.)?google', url.hostname):
            m = re.search(r'/edition/[^/]+/([^?/]+)', url.path)
            if m:
                id = m.group(1)

        return id

    def create_from_id(self, id):
        return GoogleBooksLinks(id)

class GoogleBooksLinks(LinkFinder):

    icon = 'https://upload.wikimedia.org/wikipedia/commons/thumb/5/53/Google_%22G%22_Logo.svg/16px-Google_%22G%22_Logo.svg.png'
    name = 'Google Books'

    def get_catalog_url(self):
        return f'https://books.google.com/books?id={self.id}'

    def get_links_internal(self, page):
        links = super().get_links_internal(page)
        links += [
            {
                'type': 'document',
                'format': 'application/pdf',
                'url': f'https://books.google.com/books/download/?id={self.id}&output=pdf',
                'title': 'Google Books PDF'
            }
        ]
        return links

class HathiTrustFactory(LinkFinderFactory):

    def id_from_url(self, url):
        id = None
        if url.hostname == 'hdl.handle.net':
            path_parts = url.path.split('/')
            if '2027' in path_parts:
                return path_parts[-1]

        elif url.hostname == 'babel.hathitrust.org':
            try:
                id = urllib.parse.parse_qs(url.query)['id'][0]
            except KeyError:
                pass

        if id:
            return id

        return None

    def create_from_id(self, id):
        return HathiTrustLinks(id)

class HathiTrustLinks(LinkFinder):

    icon = 'https://upload.wikimedia.org/wikipedia/commons/thumb/2/23/HathiTrust_icon_16px.svg/16px-HathiTrust_icon_16px.svg.png'
    name = 'Hathi Trust'

    def get_catalog_url(self):
        return f'https://hdl.handle.net/2027/{self.id}'

    def get_links_internal(self, page):
        links = super().get_links_internal(page)
        links += [
            # This can be JPG or PNG
            {
                'type': 'image',
                'url': f'https://babel.hathitrust.org/cgi/imgsrv/image?id={self.id};seq={page};size=full;rotation=0',
                'title': 'High-res image file (JPEG/PNG)'
            },
            self.make_tiff_link(
                f'https://babel.hathitrust.org/cgi/imgsrv/image?id={self.id}&size=full&format=image%2Ftiff&seq={page}'
            )
        ]
        return links

class IiifFinder():

    def get_manifest_url(self, page):
        raise NotImplementedError

    def get_mirador_url(self, page):
        raise NotImplementedError

    def get_manifest(self, page):
        manifest_url = self.get_manifest_url(page)
        print(manifest_url)
        try:
            r = requests.get(manifest_url)
            r.raise_for_status()
        except Exception as e:
            logging.error(traceback.format_exc())
            return

        data = r.json()

        if 'sequences' not in data and 'pages' in data:
            # this is some kind of indirect manifest
            # for example, Oregon Newspapers

            try:
                page_data = data['pages'][page - 1]
            except KeyError:
                logging.error(f'IIIF: No page item found for page {page}')
                return None

            try:
                manifest_url = page_data['url']
            except KeyError:
                logging.error(f'IIIF: No URL found for page {page}')
                return None

            try:
                r = requests.get(manifest_url)
                r.raise_for_status()
            except Exception as e:
                logging.error(traceback.format_exc())
                return

            data = r.json()

        return data

    @staticmethod
    def _item_is_type(item, expected_type):
        if 'type' not in item:
            logging.error("No type in item")
            return False
        return item['type'].lower() == expected_type.lower()

    @staticmethod
    def _get_items_of_type(this_item, expected_type):
        try:
            sub_items = this_item['items']
        except KeyError:
            logging.error(f"Expected a subitem of type: {expected_type}")
            return None

        if not isinstance(sub_items, list) or len(sub_items) == 0:
            logging.error("Subitems not a non-empty list")
            return None

        if not IiifFinder._item_is_type(sub_items[0], expected_type):
            logging.error(f"Item not of type {expected_type}: {sub_items[0]}")
            return None

        return sub_items

    def _get_iiif_body_from_presentation_3_manifest(self, manifest, page):

        try:
            items = manifest['items']
        except KeyError:
            logging.error("IIIF Presentation 3 manifests must have items")
            return None

        if len(items) <= page:
            logging.error(f"No item for page {page} (have only {len(items)} items)")

        item = items[0]

        if 'type' not in item or item['type'].lower() != 'canvas':
            logging.error("IIIF Presentation 3 items must be canvases")
            return None

        annotation_page = self._get_items_of_type(item, 'AnnotationPage')

        if not annotation_page:
            logging.debug("Expected an annotation page item")
            return None

        annotation = self._get_items_of_type(annotation_page[0], 'annotation')

        if not annotation:
            logging.debug("Expected annotation items")
            return None

        try:
            return annotation[0]['body'] #
        except KeyError:
            logging.error("Cant find body for annotation")

        return None

    def _get_iiif_info_from_presentation_3_manifest(self, manifest, page):
        body = self._get_iiif_body_from_presentation_3_manifest(manifest, page)

        if not body:
            return None

        return body['service'][0]['@id'] + '/info.json'

    def _get_iiif_info_from_presentation_2_manifest(self, manifest, page):

        try:
            images = manifest['sequences'][0]['canvases'][page - 1]['images']
        except KeyError:
            logging.error("IIIF manifest doesn't have canvas images")
            return None

        try:
            service_id = images[0]['resource']['service']['@id']
        except KeyError:
            logging.error("IIIF image doesn't have service ID")
            return None

        return service_id + '/info.json'

    @staticmethod
    def _iiif_manifest_contexts(manifest):
        try:
            contexts = manifest['@context']
        except KeyError:
            logging.error("No @context in IIIF manifest?")
            raise

        if not isinstance(contexts, list):
            contexts = [contexts]

        return contexts

    def _get_iiif_info_url_from_manifest(self, manifest, page):

        contexts = IiifFinder._iiif_manifest_contexts(manifest)

        if 'http://iiif.io/api/presentation/2/context.json' in contexts:
            return self._get_iiif_info_from_presentation_2_manifest(manifest, page)
        elif 'http://iiif.io/api/presentation/3/context.json' in contexts:
            return self._get_iiif_info_from_presentation_3_manifest(manifest, page)

        logging.error(f"Cannot get info for unhandled contexts: {contexts}")

        return None

    def _get_iiif_image_from_presentation_3_manifest(self, manifest, page):
        body = self._get_iiif_body_from_presentation_3_manifest(manifest, page)

        if not body:
            return None
        # The ID is the URL
        return body['id']

    def _get_iiif_image_from_presentations_2_manifest(self, manifest, page):
        # this works for presentation 2, but not sure if others too
        try:
            sequences = manifest['sequences']
        except KeyError:
            logging.debug('IIIF: No sequences found')
            return None

        try:
            images = sequences[0]['canvases'][page - 1]['images']
        except KeyError:
            return None

        try:
            jpg = next(i for i in images if i['resource']['format'] == 'image/jpeg')
        except StopIteration:
            # No JPEG
            return None

        url = jpg['resource']['@id']

        if not url.endswith('.jpg'):
            url += '/full/full/0/default.jpg'

        # sometimes there's a resolution preset
        # replace it with full
        url = re.sub(r'(?<=/full/)!\d+,\d+(?=/\d/)', 'full', url)

        return url

    def get_iiif_info_url(self, page, manifest=None):

        if manifest is None:
            manifest = self.get_manifest(page)

        if not manifest:
            logging.error(f'IIIF: Manifest not found for page {page}')

        info = self._get_iiif_info_url_from_manifest(manifest, page)
        logging.debug(f"IIIF info URL: {info}")
        if info is not None:
            return info

        return None

    def _get_iiif_rendering_of_format(self, manifest, formats):

        try:
            renderings = manifest['rendering']
        except KeyError:
            logging.debug('IIIF: No renderings found')
            return None

        if not isinstance(renderings, list):
            renderings = [renderings]

        def rendering_is_of_format(r):
            return 'format' in r and r['format'] in formats

        try:
            pdf_rendering = next(r for r in renderings if rendering_is_of_format(r))
        except StopIteration:
            return None

        if 'url' in pdf_rendering:
            return pdf_rendering['url']

        for key in ['@id', 'id']:
            if key in pdf_rendering:
                return pdf_rendering[key]

        return None

    def get_pdf_url(self, manifest):
        # Bielefeld mis-spells this
        pdf_formats = ['application/pdf', 'applocation/pdf']
        return self._get_iiif_rendering_of_format(manifest, pdf_formats)

    def get_jpg_url(self, manifest, page):

        contexts = IiifFinder._iiif_manifest_contexts(manifest)

        if 'http://iiif.io/api/presentation/3/context.json' in contexts:
            return self._get_iiif_image_from_presentation_3_manifest(manifest, page)

        if 'http://iiif.io/api/presentation/2/context.json' in contexts:
            return self._get_iiif_image_from_presentations_2_manifest(manifest, page)

        logging.error(f'Cannot get image from unhandled contexts: {contexts}')
        return None

    def get_iiif_links(self, page):
        manifest = self.get_manifest(page)
        links = []

        if not manifest:
            logging.error('IIIF: No manifest?')
            return links

        pdf_url = self.get_pdf_url(manifest)
        if pdf_url is not None:
            links.append({
                'type': 'document',
                'format': 'application/pdf',
                'url': pdf_url,
                'title': 'PDF'
            })

        jpg_url = self.get_jpg_url(manifest, page)
        if jpg_url is not None:
            links.append({
                'type': 'image',
                'format': 'image/jpeg',
                'url': jpg_url,
                'title': 'High-res JPEG',
                'highres': True
            })

        if manifest:
            manifest_url = self.get_manifest_url(page)
            links.append({
                'type': 'iiif-manifest',
                'title': 'IIIF Manifest',
                'url': manifest_url
            })

        if page is not None:
            iiif_url = self.get_iiif_info_url(page, manifest)
            print(f"Page {page} {iiif_url}")
            if iiif_url:
                links.append({
                    'type': 'iiif',
                    'title': 'IIIF Page Info',
                    'url': iiif_url
                })

        try:
            mirador = self.get_mirador_url(page)
            if mirador:
                links.append({
                    'type': 'viewer',
                    'format': 'mirador',
                    'url': mirador,
                    'title': 'View in Mirador'
                })
        except NotImplementedError:
            pass

        return links


class NlsLinkFactory(LinkFinderFactory):

    def id_from_url(self, url):
        if url.hostname == 'digital.nls.uk':
            m = re.search(r'^/([0-9]+)', url.path )
            if m:
                return m.group(1)

        return None

    def create_from_id(self, id):
        return NlsLinks(id)

class NlsLinks(LinkFinder, IiifFinder):

    icon = 'https://upload.wikimedia.org/wikipedia/commons/3/37/National_Library_of_Scotland_%28NLS%29_favicon_16px.png'
    name = 'NLS'

    def get_catalog_url(self):
        return f'https://digital.nls.uk/{self.id}'

    def get_manifest_url(self, page):
        return f'https://view.nls.uk/manifest/{self.id[0:4]}/{self.id[4:8]}/{self.id}/manifest.json'

    def get_links_internal(self, page):
        links = super().get_links_internal(page)
        links += self.get_iiif_links(page)
        links += [
            {
                'type': 'catalog',
                'url': f'https://view.nls.uk/mirador/{self.id}',
                'title': 'View in Mirador viewer'
            }
        ]
        return links

class OpenOniFactory(LinkFinderFactory):
    def __init__(self, domain, name):
        super().__init__()
        self.domain = domain
        self.name = name

    def key(self):
        return super().key() + self.domain

    def id_from_url(self, url):
        if url.hostname == self.domain:
            m = re.search(r'lccn/(.*\/ed-\d+)', url.path)
            return m.group(1) if m else None

        return None

    def create_from_id(self, id):
        return OpenOniLinks(id, self.domain, self.name)

class OpenOniLinks(LinkFinder, IiifFinder):
    icon = 'https://upload.wikimedia.org/wikipedia/commons/thumb/7/7d/Crystal_128_knode.svg/16px-Crystal_128_knode.svg.png'

    def __init__(self, id, domain, name):
        LinkFinder.__init__(self, id)
        IiifFinder.__init__(self)
        self.domain = domain
        self.name = name

    def get_manifest_url(self, page):
        return f'https://{self.domain}/lccn/{self.id}.json'

    def get_catalog_url(self):
        return f'https://{self.domain}/lccn/{self.id}'

    def get_links_internal(self, page):
        links = super().get_links_internal(page)
        links += self.get_iiif_links(page)
        links += [
			{
                'type': 'image',
				'url': f'https://{self.domain}/lccn/{self.id}/seq-{page}.jp2',
				'title': f'JP2 at {self.name}'
			},
			{
                'type': 'document',
				'url': f'https://{self.domain}/lccn/{self.id}/seq-{page}.pdf',
				'title': f'PDF at {self.name}'
			}
        ]
        return links

class GallicaFactory(LinkFinderFactory):
    domain = 'gallica.bnf.fr'

    def id_from_url(self, url):
        if url.hostname == self.domain:
            m = re.search(r'(ark:/\d+/[^/]+)', url.path)
            if m:
                return m.group(1)
        return None

    def create_from_id(self, id):
        return GallicaLinks(id)

class GallicaLinks(LinkFinder, IiifFinder):
    # This appears to be an IIIF Presentation 2 system

    name = "BnF Gallica"
    domain = 'gallica.bnf.fr'
    icon = 'https://upload.wikimedia.org/wikipedia/commons/thumb/8/84/Logo_BnF.svg/31px-Logo_BnF.svg.png'

    def get_catalog_url(self):
        return f'https://{self.domain}/{self.id}'

    def get_manifest_url(self, page):
        return f'https://{self.domain}/iiif/{self.id}/manifest.json'

    def get_links_internal(self, page):
        links = super().get_links_internal(page)
        links += self.get_iiif_links(page)
        return links

class ModernistJournalsFactory(LinkFinderFactory):
    domain = 'modjourn.org'
    domain_brown = 'repository.library.brown.edu'

    def id_from_url(self, url):
        if url.hostname == self.domain:
            m = re.search(r'/issue/([^/]+)', url.path)
            if m:
                return m.group(1)

        elif url.hostname == self.domain_brown:
            m = re.search(r'/(?:item)/([^/]+)', url.path)
            if m:
                return m.group(1).replace(':', '')
        return None

    def create_from_id(self, id):
        return ModernistJournalsLinks(id)

class ModernistJournalsLinks(LinkFinder, IiifFinder):
    name = "Modernist Journals"
    domain = 'modjourn.org'
    icon = 'https://upload.wikimedia.org/wikipedia/commons/thumb/7/7d/Crystal_128_knode.svg/16px-Crystal_128_knode.svg.png'

    def get_catalog_url(self):
        return f'https://{self.domain}/issue/{self.id}'

    def get_brown_id(self):

        # Doesn't need it
        if ':' in self.id:
            return self.id

        m = re.match(r'(\D+)(\d+)', self.id)
        if m:
            return f'{m.group(1)}:{m.group(2)}'

    def get_links_internal(self, page):
        links = super().get_links_internal(page)
        links += self.get_iiif_links(page)
        return links

    def get_manifest_url(self, page):
        return f'https://repository.library.brown.edu/iiif/presentation/{self.get_brown_id()}/manifest.json'

class GoobiLinkFactory(LinkFinderFactory):
    def __init__(self, api, name, prefix, icon) -> None:
        super().__init__()
        self.name = name
        self.prefix = prefix
        self.icon = icon
        self.api = api

        url = urllib.parse.urlparse(prefix)
        self.domain = url.hostname

    def key(self):
        return super().key() + self.prefix

    def _id_from_resolver_url(self, url):
        if '/resolver' not in url.path:
            return None

        logging.error(url)

        try:
            urn = urllib.parse.parse_qs(url.query)['urn'][0]
        except KeyError:
            return None

        mets_url = f'http://{self.domain}/viewer/metsresolver?urn={urn}'
        r = requests.get(mets_url)
        if r.status_code != 200:
            logging.error('')
            return None

        reader = mets.MetsReader()
        mets_info = reader.read(r.content)

        rec_id = mets_info.find('.//mods:recordIdentifier')
        if rec_id is not None:
            return rec_id.text

        return None

    def id_from_url(self, url):
        if url.hostname == self.domain:
            m = re.search(r'/(?:image|!\w+)/([^/]+)', url.path)
            if m:
                return m.group(1)

            id = self._id_from_resolver_url(url)
            if id:
                return id
        return None

    def create_from_id(self, id):
        return GoobiLinks(self.api, self.name, self.prefix, self.icon, id)

class GoobiLinks(LinkFinder, IiifFinder):

    def __init__(self, api, name, prefix, icon, id):
        LinkFinder.__init__(self, id)
        IiifFinder.__init__(self)
        self.name = name
        self.prefix = prefix
        self.icon = icon
        self.api = api

    def get_catalog_url(self):
        return f'{self.prefix}/image/{self.id}'

    def get_manifest_url(self, page):

        if self.api == 1:
            return f'{self.prefix}/api/v1/records/{self.id}/manifest'

        # ??? bielefeld doing their own thing?
        return f'{self.prefix}/rest/iiif/manifests/{self.id}/manifest/'

    def get_links_internal(self, page):
        links = super().get_links_internal(page)
        links += self.get_iiif_links(page)
        return links

class LocFactory(LinkFinderFactory):
    domain = 'loc.gov'

    def _get_id_from_resource(self, res_id):

        resource_url =  f'https://www.loc.gov/resource/{res_id}?fo=json'

        r = requests.get(resource_url)
        if r.status_code != 200:
            logging.error(f'No resource info found: {res_id} -> ret code {r.status_code}')
            return None

        res_data = r.json()

        try:
            id = res_data['item']['id']
        except KeyError:
            logging.error(f'No id found for resource: {res_id}')
            return None

        m = re.search(r'/item/(.+)', id)
        if m:
            return m.group(1).rstrip('/')

        return None

    def id_from_url(self, url):
        if url.hostname.endswith(self.domain):

            m = re.search(r'/item/([^?/]+)', url.path)
            if m:
                return m.group(1)

            # serials
            m = re.search(r'/resource/(sn[^?/]+/\d{4}-\d{2}-\d{2}/ed-\d+)', url.path)
            if m:
                id = self._get_id_from_resource(m.group(1))
                return id

            m = re.search(r'/resource/([^?/]+)', url.path)
            if m:
                id = self._get_id_from_resource(m.group(1))
                return id

        return None

    def create_from_id(self, id):
        return LocLinks(id)

class LocLinks(LinkFinder):

    name = 'Library of Congress'
    icon = 'https://upload.wikimedia.org/wikipedia/commons/9/9e/Library_of_Congress_favicon.png'

    def __init__(self, id) -> None:
        super().__init__(id)

        item_url = f'https://www.loc.gov/item/{self.id}?fo=json'
        r = requests.get(item_url)
        r.raise_for_status()

        self.item_data = r.json()

    def get_catalog_url(self):
        return f'https://www.loc.gov/item/{self.id}'

    def _get_page_files(self, page):
        try:
            return self.item_data['resources'][0]['files'][page - 1]
        except KeyError:
            logging.error('LOC: Failed to get item page resource: {self.id}, {page}')
        return None

    def get_largest_jpg(self, page):
        page_files = self._get_page_files(page)
        if not page_files:
            return None

        biggest_jpg = None
        for file in page_files:
            if file['mimetype'] == 'image/jpeg':
                if (biggest_jpg is None or
                    biggest_jpg['width'] < file['width']):
                    biggest_jpg = file

        if biggest_jpg is not None:
            return biggest_jpg['url']

        return None

    def get_iiif(self, page):
        page_files = self._get_page_files(page)
        if not page_files:
            return None

        for file in page_files:
            if 'info' in file:
                return file['info']
        return None

    def get_file_with_mimetype(self, page, mimetype):
        page_files = self._get_page_files(page)
        if not page_files:
            return None

        for file in page_files:
            if file['mimetype'] == mimetype:
                return file['url']
        return None

    def get_links_internal(self, page):
        links = super().get_links_internal(page)

        biggest_jpg = self.get_largest_jpg(page)
        if biggest_jpg is not None:
            links += [{
                'icon': self.icon,
                'type': 'image',
                'highres': True,
                'format': 'image/jpeg',
                'url': biggest_jpg,
                'title': f'JPEG at {self.name}'
            }]

        iiif = self.get_iiif(page)
        if iiif:
            links += [{
                'type': 'iiif',
                'url': iiif
            }]

        tiff = self.get_file_with_mimetype(page, 'image/tiff')
        if tiff is not None:
            links += [{
                'icon': self.icon,
                'type': 'image',
                'highres': True,
                'format': 'image/tiff',
                'url': tiff,
                'title': f'TIFF at {self.name}'
            }]

        jp2 = self.get_file_with_mimetype(page, 'image/jp2')
        if jp2 is not None:
            links += [{
                'icon': self.icon,
                'type': 'image',
                'highres': True,
                'format': 'image/jp2',
                'url': jp2,
                'title': f'JP2 at {self.name}'
            }]
        return links
class DigitalBodleianLinks(LinkFinder, IiifFinder):
    # This appears to be an IIIF Presentation 2 system

    name = 'Digital Bodleian'
    domain = 'digital.bodleian.ox.ac.uk'
    icon = 'https://www.bodleian.ox.ac.uk/sites/default/files/styles/favicon-16x16/public/bodreader/site-favicon/bod-favicon.png'

    def get_catalog_url(self):
        return f'https://{self.domain}/objects/{self.id}'

    def get_manifest_url(self, page):
        return f'https://iiif.bodleian.ox.ac.uk/iiif/manifest/{self.id}.json'

    def get_links_internal(self, page):
        links = super().get_links_internal(page)
        links += self.get_iiif_links(page)

        links += [
            {
                'url': f'https://iiif.bodleian.ox.ac.uk/iiif/mirador/{self.id}',
                'title': 'View in Mirador',
                'type': 'viewer'
            },
            {
                'url': f'https://iiif.bodleian.ox.ac.uk/iiif/viewer/{self.id}',
                'title': 'View in \'Universal\' viewer',
                'type': 'viewer'
            }
        ]

        return links

class DigitalBodleianFactory(LinkFinderFactory):

    domain = 'digital.bodleian.ox.ac.uk'

    def id_from_url(self, url):
        if url.hostname.endswith(self.domain):
            m = re.search(r'/objects/([^?/]+)', url.path)
            if m:
                return m.group(1)
        return None

    def create_from_id(self, id):
        return DigitalBodleianLinks(id)

class CambridgeDigitalLinks(LinkFinder, IiifFinder):
    # This appears to be an IIIF Presentation 2 system

    name = 'Cambridge Digital Library (CUDL)'
    domain = 'cudl.lib.cam.ac.uk'
    icon = 'https://upload.wikimedia.org/wikipedia/commons/thumb/8/8a/University_of_Cambridge_coat_of_arms.svg/16px-University_of_Cambridge_coat_of_arms.svg.png'

    def get_catalog_url(self):
        return f'https://{self.domain}/view/{self.id}'

    def get_manifest_url(self, page):
        return f'https://{self.domain}/iiif/{self.id}'

    def get_links_internal(self, page):
        links = super().get_links_internal(page)
        links += self.get_iiif_links(page)

        links += [
            {
                'icon': self.icon,
                'url': f'https://images.lib.cam.ac.uk/content/images/{self.id}-000-{page:05d}.jpg',
                'title': 'High-res JPG',
                'type': 'image',
                'highres': True,
                'format': 'image/jpeg',
            },
            {
                'icon': self.icon,
                'url': f'https://cudl.lib.cam.ac.uk/mirador/{self.id}/1',
                'title': 'View in Mirador',
                'type': 'viewer'
            }
        ]

        return links

class CambridgeDigitalFactory(LinkFinderFactory):

    domain = 'cudl.lib.cam.ac.uk'

    def id_from_url(self, url):
        if url.hostname.endswith(self.domain):
            m = re.search(r'/view/([^?/]+)', url.path)
            if m:
                return m.group(1)
        return None

    def create_from_id(self, id):
        return CambridgeDigitalLinks(id)

class DziLinker():

    def get_dzi_pageid(self, page):
        raise NotImplementedError

    def get_dzi_img_root(self, page):
        raise NotImplementedError

    def get_dzi_url(self, page):
        raise NotImplementedError

    def get_dzi_data(self, page):

        dziurl = self.get_dzi_url(page)
        img_root = self.get_dzi_img_root(page)

        if not dziurl or not img_root:
            return None

        r = self.session.get(dziurl)
        r.raise_for_status()

        doc = lxml.etree.fromstring(r.content)

        nsmap = {
            'dz': 'http://schemas.microsoft.com/deepzoom/2009'
        }

        # Insert the 3rd party URL root
        image = doc.xpath('//dz:Image', namespaces=nsmap)[0]
        image.attrib['Url'] = img_root

        return lxml.etree.tostring(doc).decode('utf-8')

class BritishLibraryManuscriptLinks(LinkFinder, DziLinker):

    name = 'British Library Manuscripts'
    domain = 'www.bl.uk'
    icon = 'https://upload.wikimedia.org/wikipedia/commons/thumb/f/f7/BritishLibrary.svg/8px-BritishLibrary.svg.png'

    def __init__(self, id) -> None:
        super().__init__(id)

        self.session = requests.Session()
        headers = {
            'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64; rv:95.0) Gecko/20100101 Firefox/95.0'
        }
        self.session.headers.update(headers)
        self.pl = None

    def get_catalog_url(self):
        return f'https://www.bl.uk/manuscripts/FullDisplay.aspx?ref={self.id}'

    def get_viewer_url(self, page=None) -> str:

        if page is not None:
            pageid = self.get_dzi_pageid(page)
        else:
            pageid = self.id

        if pageid:
            return f'https://www.bl.uk/manuscripts/Viewer.aspx?ref={pageid}'
        return None

    def get_page_list(self):

        if self.pl:
            return self.pl

        r = self.session.get(self.get_viewer_url(None))
        r.raise_for_status()

        doc = lxml.html.fromstring(r.content)
        pl_elem = doc.get_element_by_id('PageList')
        values = pl_elem.attrib['value'].split('||')
        self.pl = [v for v in values if v != '##']
        return self.pl

    def get_dzi_pageid(self, page):
        pl = self.get_page_list()
        try:
            return pl[page - 1]
        except KeyError:
            pass
        return None

    def get_dzi_img_root(self, page):
        pageid = self.get_dzi_pageid(page)
        if pageid:
            return f'https://www.bl.uk/manuscripts/Proxy.ashx?view={pageid}_files/'
        return None

    def get_dzi_url(self, page):
        pageid = self.get_dzi_pageid(page)
        if pageid:
            return f'https://www.bl.uk/manuscripts/Proxy.ashx?view={pageid}.xml'
        return None

    def get_links_internal(self, page):
        links = super().get_links_internal(page)
        links += [
            {
                'url': self.get_viewer_url(page),
                'title': 'Page in viewer',
            },
            {
                'title': 'DZI data',
                'type': 'dzi',
                'data': self.get_dzi_data(page)
            }
        ]
        return links

class BritishLibraryManuscriptFactory(LinkFinderFactory):

    def id_from_url(self, url):
        if url.hostname.endswith(BritishLibraryManuscriptLinks.domain):
            if url.path.startswith('/manuscripts/'):
                qs = urllib.parse.parse_qs(url.query)
                if qs and 'ref' in qs:
                    return qs['ref'][0]
        return None

    def create_from_id(self, id):
        return BritishLibraryManuscriptLinks(id)

class YaleCollectionsLinks(LinkFinder, IiifFinder):

    name = 'Yale Digital Collections'
    domain = 'collections.library.yale.edu'
    icon = 'https://upload.wikimedia.org/wikipedia/commons/thumb/2/2f/Yale_favicon_64px.png/16px-Yale_favicon_64px.png'

    def get_catalog_url(self):
        return f'https://{self.domain}/catalog/{self.id}'

    def get_manifest_url(self, page):
        return f'https://{self.domain}/manifests/{self.id}'

    def get_mirador_url(self, page):
       return f'https://{self.domain}/mirador/{self.id}'

    def get_links_internal(self, page):
        links = super().get_links_internal(page)
        links += self.get_iiif_links(page)
        return links
class YaleCollectionsFactory(LinkFinderFactory):

    def id_from_url(self, url):
        if url.hostname.endswith(YaleCollectionsLinks.domain):
            if url.path.startswith('/catalog/'):
                # https://collections.library.yale.edu/catalog/15947166
                parts = url.path.split('/')
                if len(parts) > 2:
                    return parts[2]
        return None

    def create_from_id(self, id):
        return YaleCollectionsLinks(id)

class MiamiUniversityLinks(LinkFinder, IiifFinder):

    name = 'Miami University Digital Collections'
    domain = 'digital.lib.miamioh.edu'
    icon = 'https://upload.wikimedia.org/wikipedia/commons/thumb/7/72/Eo_circle_light-blue_letter-m.svg/16px-Eo_circle_light-blue_letter-m.svg.png'

    def get_catalog_url(self):
        cat_id = self.id.replace('/', '/id/')
        return f'https://{self.domain}/digital/collection/{cat_id}'

    def get_manifest_url(self, page):
        # HTTPS cert errors for some reason
        return f'http://{self.domain}/iiif/info/{self.id}/manifest.json'

    def get_links_internal(self, page):
        links = super().get_links_internal(page)
        links += self.get_iiif_links(page)
        return links
class MiamiUniversityFactory(LinkFinderFactory):

    def id_from_url(self, url):
        if url.hostname.endswith(MiamiUniversityLinks.domain):
            if url.path.startswith('/digital/collection/'):
                # https://digital.lib.miamioh.edu/digital/collection/postcards/id/121686/
                # id → postcards/121686
                parts = url.path.split('/')
                if len(parts) > 5 and parts[4] == 'id':
                    return f'{parts[3]}/{parts[5]}'
        return None

    def create_from_id(self, id):
        return MiamiUniversityLinks(id)

from .url_to_id_cache import UrlToIdCache

class FactoryRegistry():

    registry = {}

    def register(self, factory):
        key = factory.key()
        self.registry[key] = factory

    def get_factory(self, key):

        try:
            return self.registry[key]
        except KeyError:
            pass
        return None

factories = [
    IaLinkFactory(),
    HathiTrustFactory(),
    GoogleBooksFactory(),
    NlsLinkFactory(),
    GallicaFactory(),
    ModernistJournalsFactory(),
    LocFactory(),
    DigitalBodleianFactory(),
    CambridgeDigitalFactory(),
    BritishLibraryManuscriptFactory(),
    YaleCollectionsFactory(),
    MiamiUniversityFactory(),
    GoobiLinkFactory(
        api = 'bielefeld',
        name = "Uni. Bielefeld",
        prefix = 'http://ds.ub.uni-bielefeld.de/viewer',
        icon = 'https://upload.wikimedia.org/wikipedia/commons/thumb/2/26/Universit%C3%A4t_Bielefeld_Logo_-_16px.svg/16px-Universit%C3%A4t_Bielefeld_Logo_-_16px.svg.png'
    ),
    GoobiLinkFactory(
        api = 1,
        name = "Humboldt-Uni. Berlin (HUB)",
        prefix = 'http://www.digi-hub.de/viewer',
        icon = 'https://upload.wikimedia.org/wikipedia/commons/6/63/Humboldt-Universit%C3%A4t_zu_Berlin_-_favicon_16px.png'
    ),
    GoobiLinkFactory(
        api = 1,
        name = "Digitale Landesbibliothek Berlin (ZLB)",
        prefix = 'https://digital.zlb.de/viewer',
        icon = 'https://upload.wikimedia.org/wikipedia/commons/thumb/2/2c/Corporate_Design_ZLB.JPG/30px-Corporate_Design_ZLB.JPG'
    ),
    OpenOniFactory(
        'newspapers.digitalnc.org',
        'Digital NC'
    ),
    OpenOniFactory(
        'oregonnews.uoregon.edu',
        'Historic Oregon Newspapers'
    ),
    OpenOniFactory(
        'panewsarchive.psu.edu',
        'Pennsylvania Newspaper Archive'
    ),
    OpenOniFactory(
        'nebnewspapers.unl.edu',
        'Nebraska Newspapers'
    )
]

registry = FactoryRegistry()

for factory in factories:
    registry.register(factory)

url_cache = UrlToIdCache()

domainDenylist = [
    'pdfreaders.org',
    'dbnl.org',
    'oclc.org',
    'creativecommons.org',
    'd-nb.info',
    'toolforge.org',
    'wmcloud.org',
    'viaf.org',
    'wikidata.org',
    'wikimedia.org'
]

# We will cache at multiple levels, with the hope that if a URL is used a lot, it
# will hit the outer caches first, while the inner caches are more selective, but
# protect the most expensive functions

lru_size = 1024

# This will handle a lot of spurious URLs, but each one is just a bool response
@lru_cache(lru_size * 10)
def check_denied(url):
    for domain in domainDenylist:
        if url.hostname.endswith(domain):
            return True
    return False

@lru_cache(lru_size * 10)
def get_url_obj(url):
    return urllib.parse.urlparse(url)

def get_finder_from_url(url, url_obj, use_cache=True):
    finder = None
    cached = url_cache.getForUrl( url ) if use_cache else None

    if cached:
        factory = registry.get_factory(cached['factory'])

        id = cached['id']
        if id:
            finder = factory.create_from_id(id)
    else:
        for key, factory in registry.registry.items():

            finder = factory.create_from_url(url_obj)

            if finder:
                url_cache.addToCache(url, finder.id, key)
                break
    return finder

# we first check if the URL/page combo is cached though a simple LRU cache
# which will handle reloads of the same page again and again, but will eventually
# expire, because we don't want to cache every single page forever
#
# This then defers to the inner cache which has a much more persistent cache policy
# to link URLs to works (but not pages themselves)
@lru_cache(lru_size)
def get_links_from_url_inner(url, url_obj, page, use_cache=True):
    finder = get_finder_from_url(url, url_obj, use_cache)

    links = []
    if finder:
        links = finder.get_links(page) or []
    return links

@lru_cache(lru_size)
def get_links_from_url(url, page, use_cache=True):

    url_obj = get_url_obj(url)

    # cheap checks for common URLs that we know will not lead to
    # scan hosts
    if url_obj is None or check_denied(url_obj):
        return []
    return get_links_from_url_inner(url, url_obj, page, use_cache)

def get_links(iwlinks, extlinks, page):

    urls = []

    for extlink in extlinks:
        urls.append(extlink['url'])

    for iwlink in iwlinks:
        # This is a bit of hack, but not very expensive
        if iwlink['prefix'] == 'iarchive':
            urls.append('https://archive.org/details/' + iwlink['title'])

    links = []
    for url in urls:
        links += get_links_from_url(url, page)
    return links
