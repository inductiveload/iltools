
from lxml import etree
import logging

class Mets():

    def __init__(self, doc):
        self.doc = doc

    def find(self, path):
        result = self.doc.find(path, namespaces=self.doc.nsmap)
        return result

class MetsReader():

    def read(self, content):
        doc = etree.fromstring(content)
        return Mets(doc)