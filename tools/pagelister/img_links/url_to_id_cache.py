
import logging


class UrlToIdCache():
  """
  Simple cache for storing a mapping of URLs to upstream IDs

  This short-circuits quite a lot of processing for some sources,
  as it can save a network request. Since this can save a request
  per page load, that's pretty good.

  This is a bit like the @lru_cache deocrator, but one day it
  will be extended to persist to a DB or something
  """

  def __init__(self) -> None:
      self.cache = {}

  def addToCache(self, url, id, factory):

    logging.error(f'Cache update: {url} -> {id}')

    self.cache[url] = {
      'id': id,
      'factory': factory
    }

  def getForUrl(self, url):

    try:
      return self.cache[url]
    except KeyError:
      pass

    logging.error(f'Cache miss for {url}')
    return None

  def persist(self):
    raise NotImplementedError

  def load(self):
    raise NotImplementedError