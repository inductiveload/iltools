
import pytest
import urllib.parse

from img_links import links as L

def expect_url_for_type(type, expected, links, format=None):

    def match(l):
        if type != l['type']:
            return False
        if format and ('format' not in l or l['format'] != format):
            return False
        return True

    try:
        link = next(l for l in links if match(l))
    except StopIteration:
        pytest.fail(f'No matching "{type}" link found')

    assert link['url'] == expected

def check_case(c):

    factory = c['factory']

    finder = None
    if 'url' in c:
        print(f'Testing {c["url"]}')
        url = urllib.parse.urlparse(c['url'])
        finder = factory.create_from_url(url)
    else:
        assert False, "Expected a URL link"

    if finder is None:
        pytest.fail('Could not construct a LinkFinder')

    links = finder.get_links(c['page'])

    print(links)

    if 'iiif' in c:
        expect_url_for_type('iiif', c['iiif'], links, )

    if 'catalog' in c:
        expect_url_for_type('catalog', c['catalog'], links)

    if 'pdf' in c:
        expect_url_for_type('document', c['pdf'], links,
            format='application/pdf')

    if 'jpg' in c:
        expect_url_for_type('image', c['jpg'], links,
            format='image/jpeg')

    if 'mirador' in c:
        expect_url_for_type('viewer', c['mirador'], links,
            format='mirador')

def check_cases(cases):
    for c in cases:
        check_case(c)

def test_ia():
    cases = [{
        'factory': L.IaLinkFactory(),
        'url': 'https://archive.org/details/heartbreakhouseg00shaw',
        'page': 2,
        'iiif': 'https://iiif.archivelab.org/iiif/heartbreakhouseg00shaw$2/info.json',
        'catalog': 'https://archive.org/details/heartbreakhouseg00shaw',
        'jpg': 'https://archive.org/download/heartbreakhouseg00shaw/page/n1.jpg'
    }]

    check_cases(cases)

def test_nls():
    cases = [{
        'factory': L.NlsLinkFactory(),
        'url': 'https://digital.nls.uk/104184887',
        'page': 2,
        'iiif': 'https://view.nls.uk/iiif/1085/2011/108520113.5/info.json',
        'catalog': 'https://digital.nls.uk/104184887',
        # 'mirador':
    }]

    check_cases(cases)

def test_hathi():

    cases = [{
        'factory': L.HathiTrustFactory(),
        'url': 'https://babel.hathitrust.org/cgi/pt?id=mdp.39015016411046',
        'page': 2,
        'catalog': 'https://hdl.handle.net/2027/mdp.39015016411046',
        # 'mirador':
    }]

    check_cases(cases)

def test_bielefeld():

    factory = L.GoobiLinkFactory(
        name = "Uni. Bielefeld",
        api = 'bielefeld',
        prefix = 'http://ds.ub.uni-bielefeld.de/viewer',
        icon = 'https://upload.wikimedia.org/wikipedia/commons/thumb/2/26/Universit%C3%A4t_Bielefeld_Logo_-_16px.svg/16px-Universit%C3%A4t_Bielefeld_Logo_-_16px.svg.png'
    )

    c = [{
        'factory': factory,
        'url': 'http://ds.ub.uni-bielefeld.de/viewer/!image/2002572_002/1/LOG_0003/',
        'page': 5,
        'iiif': 'http://ds.ub.uni-bielefeld.de/viewer/rest/image/2002572_002/00000005.tif/info.json',
        'catalog': 'http://ds.ub.uni-bielefeld.de/viewer/image/2002572_002',
        'pdf': 'http://ds.ub.uni-bielefeld.de/viewer/rest/pdf/mets/2002572_002.xml/1Bd2St_1765.pdf?watermarkText=http%3A%2F%2Fds.ub.uni-bielefeld.de%2Fviewer%2Fimage%2F2002572_002%2F1%2F'
    } ]

    check_cases(c)

def test_google():

    cases = [{
        'factory': L.GoogleBooksFactory(),
        'url': 'https://www.google.com/books/edition/Os_Lusiadas/Ln4NAAAAQAAJ',
        'page': 1,
        'catalog': 'https://books.google.com/books?id=Ln4NAAAAQAAJ'
    }]

    check_cases(cases)

def test_loc():

    cases = [{
        'factory': L.LocFactory(),
        'url': 'https://www.loc.gov/resource/rbc0001.2006gen32405',
        'page': 1,
        'catalog': 'https://www.loc.gov/item/03032405'
    }]

    check_cases(cases)

def test_loc_serial():

    cases = [{
        'factory': L.LocFactory(),
        'url': 'https://www.loc.gov/resource/sn83030213/1859-08-31/ed-1',
        'page': 1,
        'catalog': 'https://www.loc.gov/item/sn83030213/1859-08-31/ed-1'
    }]

    check_cases(cases)

def test_modjourn():

    cases = [
        {
            'factory': L.ModernistJournalsFactory(),
            'url': 'https://modjourn.org/issue/bdr563779/',
            'page': 1,
            'catalog': 'https://modjourn.org/issue/bdr563779'
        },
        {
            'factory': L.ModernistJournalsFactory(),
            'url': 'https://repository.library.brown.edu/studio/item/bdr:568723/PDF/',
            'page': 1,
            'catalog': 'https://modjourn.org/issue/bdr568723'
        }
    ]

    check_cases(cases)

def test_yale():

    cases = [
        {
            'factory': L.YaleCollectionsFactory(),
            'url': 'https://collections.library.yale.edu/catalog/15947166',
            'page': 1,
            'catalog': 'https://collections.library.yale.edu/catalog/15947166',
            'iiif': 'https://collections.library.yale.edu/iiif/2/15947294/info.json',
            'mirador': 'https://collections.library.yale.edu/mirador/15947166',
            'jpg': 'https://collections.library.yale.edu/iiif/2/15947294/full/full/0/default.jpg'
        }
    ]

    check_cases(cases)

def test_miami():

    cases = [
        {
            'factory': L.MiamiUniversityFactory(),
            'url': 'https://digital.lib.miamioh.edu/digital/collection/wshakespeare/id/14548',
            'page': 1,
            'catalog': 'https://digital.lib.miamioh.edu/digital/collection/wshakespeare/id/14548',
            'iiif': 'https://cdm17240.contentdm.oclc.org/digital/iiif/wshakespeare/13618/info.json',
            'jpg': 'https://cdm17240.contentdm.oclc.org/digital/iiif/wshakespeare/13618/full/full/0/default.jpg'
        }
    ]

    check_cases(cases)

def test_gallica():

    cases = [
        {
            'factory': L.GallicaFactory(),
            'url': 'https://gallica.bnf.fr/ark:/12148/bpt6k97499045/f13.image',
            'page': 1,
            'catalog': 'https://gallica.bnf.fr/ark:/12148/bpt6k97499045',
            'iiif': 'https://gallica.bnf.fr/iiif/ark:/12148/bpt6k97499045/f1/info.json',
            'jpg': 'https://gallica.bnf.fr/iiif/ark:/12148/bpt6k97499045/f1/full/full/0/native.jpg'
        }
    ]

    check_cases(cases)

def test_bodelian():

    cases = [
        {
            'factory': L.DigitalBodleianFactory(),
            'url': 'https://digital.bodleian.ox.ac.uk/objects/7396c69b-67f6-4dc0-bf07-e035bc4addbe/',
            'page': 1,
            'catalog': 'https://digital.bodleian.ox.ac.uk/objects/7396c69b-67f6-4dc0-bf07-e035bc4addbe',
            'iiif': 'https://iiif.bodleian.ox.ac.uk/iiif/image/f8591881-38de-4e9f-87ab-42f6599a6a6b/info.json',
            'jpg': 'https://iiif.bodleian.ox.ac.uk/iiif/image/f8591881-38de-4e9f-87ab-42f6599a6a6b/full/full/0/default.jpg'
        }
    ]

    check_cases(cases)


def test_cambridge():

    cases = [
        {
            'factory': L.CambridgeDigitalFactory(),
            'url': 'https://cudl.lib.cam.ac.uk/view/PR-OATES-00174/1',
            'page': 54,
            'catalog': 'https://cudl.lib.cam.ac.uk/view/PR-OATES-00174',
            'iiif': 'https://images.lib.cam.ac.uk/iiif/PR-OATES-00174-000-00054.jp2/info.json',
            'jpg': 'https://images.lib.cam.ac.uk/content/images/PR-OATES-00174-000-00054.jpg'
        }
    ]

    check_cases(cases)
