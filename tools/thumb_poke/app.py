#!/usr/bin/env python3

import flask

import os
import dotenv

import pywikibot
import logging

# static utils
from poke import poke

dotenv.load_dotenv()

pywikibot.output('Init PWB')
logger = logging.getLogger('pywiki')
logger.setLevel(logging.WARNING)

# The poker logic provider
poker = poke.Poker()

poker.start_rc_watcher()

# The web front end
app = flask.Flask(__name__)

@app.route('/favicon.ico')
def favicon():
    return flask.send_from_directory(
        os.path.join(app.root_path, 'static'),
        'favicon.ico', mimetype='image/vnd.microsoft.icon')

@app.route('/')
@app.route('/index')
def index():
    return flask.render_template('index.html',
                                 title='Thumb poke')

if __name__ == '__main__':
  app.run()
