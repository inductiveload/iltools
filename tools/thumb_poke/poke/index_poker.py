
from sys import path
import pywikibot
import pywikibot.proofreadpage
import threading
import queue
import logging
import os
import requests
import ratelimit
import backoff
import time
import traceback

import argparse

from . import index_ns

# renderfile-nonstandard = 70 per 30 seconds
RATELIMIT_COUNT = 70
RATELIMIT_PER = 30

class TooFastException(Exception):

  def __init__(self, url, *args: object) -> None:
      super().__init__(*args)
      self.url = url

class IndexPokeProcessor():
  """
  "Poke" a single index
  """
  def __init__(self, wiki, index, res) -> None:
      self.index = index

      if wiki == 'sourceswiki':
        self.site = pywikibot.Site('mul', 'wikisource')
      else:
        self.site = pywikibot.Site(wiki.replace('wikisource', ''), 'wikisource')

      self.index_ns = index_ns.get_index_ns_for_wiki(wiki)

      self.res = res

      self.session = requests.Session()
      headers = {
        'User-Agent': 'ThumbPokeBot/0.1 (https://thumbpoke.toolforge.org/)'
      }
      self.session.headers.update(headers)

      self.cache_misses = 0
      self.request_time = 0
      self.ratelimit_time = 0

  @ratelimit.sleep_and_retry
  @ratelimit.limits(calls=RATELIMIT_COUNT - 1, period=RATELIMIT_PER)
  def add_limited_request(self):
    """
    Call this to report a limited request and slow down calls to uncached results
    """
    self.cache_misses += 1

  @staticmethod
  def cache_hit(response, default=False):
    if 'X-Cache-Status' not in response.headers:
      return default
    return response.headers['X-Cache-Status'].lower().startswith('hit')

  @backoff.on_exception(backoff.expo,
    TooFastException,
    max_tries=5,
    factor=2)
  def request(self, url):

    start = time.perf_counter()

    r = self.session.head(url)
    if r.status_code == 429:
      raise TooFastException(url)

    self.request_time += time.perf_counter() - start

    # something else?
    if r.status_code != 200:
      return

    start = time.perf_counter()

    if not self.cache_hit(r):
      self.add_limited_request()

    self.ratelimit_time += time.perf_counter() - start

  def get_page_list(self, count):

    from itertools import chain

    start_len = 10
    end_len = 10

    list = range(1, min(start_len, count) + 1)

    if count > start_len:
      list = chain(list, range(count + 1, max(start_len, count - end_len) + 1, -1))

    if count > start_len + end_len:
      list = chain(list, range(start_len + 1, count - end_len + 2))

    return list

  def process(self):
    logging.info(f"Processing {self.index} @ {self.res}")

    _, ext = os.path.splitext(self.index)

    if ext not in ['.pdf', '.djvu', '.tiff']:
      # Cant handle this yet (need T167200)
      return

    start = time.perf_counter()

    index_title = self.index.split(':', 1)[-1]

    index_page = pywikibot.proofreadpage.IndexPage(self.site, 'Index:' + index_title)
    file_name = index_page.title(with_ns=False)
    file_page = pywikibot.FilePage(self.site, 'File:' + file_name)

    info = file_page.latest_file_info
    base_url = file_page.get_file_url(1024, url_param=f'page1-1024px')

    # clamp to the image size
    res = min(info.width, self.res)

    for pg in self.get_page_list(info.pagecount):
      pgurl = base_url.replace('page1-1024px', f'page{pg}-{res}px')
      logging.debug(pgurl)

      try:
        self.request(pgurl)
      except TooFastException as e:
        logging.error('Rate-limited on {index_title}/{pg}')
      except Exception as e:
        # something else, so report for debug and move on
        traceback.print_exception(e)

    total_time = time.perf_counter() - start

    if info.pagecount > 0:
      logging.info(f'Cache misses: {self.cache_misses} / {info.pagecount}')
      logging.info(f'Request time: {self.request_time:.2g}s: {self.request_time/info.pagecount:.2g}s/page')
    if self.cache_misses > 0:
      logging.info(f'Ratelimit time: {self.ratelimit_time:.2g}s: {self.ratelimit_time/self.cache_misses:.2g}s/page')
    else:
      logging.info(f'Ratelimit time: 0s')
    logging.info(f'Total time: {total_time:.2g}s')

class IndexPoker():

  thread = None
  should_stop = False
  index_q = queue.Queue()

  HI_PRIO = 1
  LO_PRIO = 2

  def __init__(self, wait) -> None:
      self.thread = threading.Thread(target=self._main)
      self.wait = wait

      self.hi_prio_res = self._get_resolutions('THUMB_POKE_HIGH_PRIORITY_RESOLUTIONS', [1024])
      self.lo_prio_res = self._get_resolutions('THUMB_POKE_LOW_PRIORITY_RESOLUTIONS', [])

  def run(self):
    self.thread.start()

  def stop(self, interrupt):

    if interrupt:
      self.should_stop = True

    self.thread.join()

  def _get_resolutions(self, key, default):
    value = os.getenv(key)

    if value is None:
      return default

    resolutions = [int(r) for r in value.split()]
    return resolutions

  def _main(self):

    while not self.should_stop:
      try:
        _, item = self.index_q.get(self.wait)
      except queue.Empty:
        break

      processor = IndexPokeProcessor(item['wiki'], item['index'], res=item['res'])
      processor.process()

      logging.info(f'Finished {item}')
      self.index_q.task_done()

    logging.info(f'Closing thread')

  def enqueue_index(self, wiki, index):

    logging.info(f"Adding to queue: {index}")

    for r in self.hi_prio_res:
      self.index_q.put((self.HI_PRIO,
        {
          'wiki': wiki,
          'index': index,
          'res': r
        }))

    for r in self.lo_prio_res:
      self.index_q.put((self.LO_PRIO,
        {
          'wiki': wiki,
          'index': index,
          'res': r
        }))
