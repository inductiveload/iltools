# WS Statistician

These programs provide some basic statistics. At present, they are used to
drive daily progress tables for the Monthly Challenge at English Wikisource.

The general idea is to look through all revisions for all pages for indexes in
a certain group: this could be the Wikisource-local Lua MC table for the month,
a category or even an explicit list of indexes. For each revision, store the proofreading status as of that revision in an auxiliary database.

Then, use that database to generate statistics for the indexes:

* Overall statistics for each index at the current time (similar to `pr_index` in
  the main DB, but doesn't need purging)
  * This uses the `update_db.py` script
* Daily statistics for the indexes of interest in aggregate.
  * This uses the `gen_stats.py` script

There is a shell script to run these scripts on a cronjob with sensible
parameters.

## Prerequisites

You must create an SQL database to contain the auxiliary proofread history data

Probably, this is on Toolforge as a user database.
`CREDENTIALUSER` comes from your `~/replica.my.cnf`:

```
sql tools
CREATE DATABASE CREDENTIALUSER__idx_hist;
```

## Running locally

Set up Python venv and install deps:

```
make create-venv
source venv/bin/activate
```

If developing locally, tunnel to the Wikisource and ToolForge user DB hosts:

```
ssh -L 4711:enwikisource.analytics.db.svc.wikimedia.cloud:3306 -L 4712:tools.db.svc.eqiad.wmflabs:3306 toolforge
```

You can use `make forward-toolforge-db` to do this if you want.

Set up the `.env` file (use the ports above). Get the user name and password
from your `~/replica.my.cnf` file on Toolforge.


```
MYSQL_USER=u<ID>
MYSQL_PASSWORD=<password>
MYSQL_MW_HOST=127.0.0.1
MYSQL_MW_PORT=4711
MYSQL_TOOLSDB_HOST=127.0.0.1
MYSQL_TOOLSDB_PORT=4712
```

You can now run the scripts, with via the shell script or directly:

```
./update_db.py -y 2022 -m 4
./gen_stats.py -y 2022 -m 4 -td
```

## Running on Toolforge

If running on Toolforge, you do not need to tunnel.

```
MYSQL_USER=u<ID>
MYSQL_PASSWORD=<password>
MYSQL_MW_HOST=enwikisource.analytics.db.svc.wikimedia.cloud
MYSQL_MW_PORT=3306
MYSQL_TOOLSDB_HOST=tools.db.svc.eqiad.wmflabs
MYSQL_TOOLSDB_PORT=3306
```

You need to run in a Kubernetes shell to get Python 3.9, which is important
for the Lua table ordering.

```
webservice --backend=kubernetes python3.9 shell
python3 -m venv $HOME/venv-k8s-py39
source $HOME/venv-k8s-py39/bin/activate
pip install --upgrade pip
pip install -r $HOME/iltools/tools/ws-statistician/requirements.txt

```

Set up the cronjob Kubernetes batch jobs:

```
kubectl apply --validate=true -f cronjobs.yaml
```

### Testing the cronjobs

```
 kubectl create job --from=cronjob/ws-stat-update-db test
 kubectl logs job/test -f
```

### View the logs

See the jobs for the cronjob:
```
$kubectl get jobs

NAME                 COMPLETIONS   DURATION   AGE
ws-stat-update-db-1620335700   1/1           12s        29m
ws-stat-update-db-1620336600   1/1           8s         14m
```

See the pods:
```
$kubectl get pods

NAME                       READY   STATUS      RESTARTS   AGE
ws-stat-update-db-1620335700-5n8bv   0/1     Completed   0          29m
ws-stat-update-db-1620336600-lhctg   0/1     Completed   0          14m
```

See the logs for a pod (select the pod you want to see)

```
kubectl logs -f ws-stat-update-db-1620336600-lhctg
```