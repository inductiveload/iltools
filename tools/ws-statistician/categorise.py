#! /usr/bin/env python

import argparse
import logging
import dotenv
import datetime
import pywikibot
import pywikibot.textlib

# our common code
import statistician

def confirm_write(dry_run, force):

    if dry_run:
        logging.info("Dry run, skipping")
        return False

    do_write = True
    if not force:
        while True:
            ans = input("Continue? [y/N] ").lower()

            if ans not in ['y', 'n']:
                logging.info("Please enter y or n")
                continue

            do_write = ans == 'y'
            if not do_write:
                logging.info("Skipping write by request")
            break

    return do_write

def main():

    parser = argparse.ArgumentParser(description='Maintain MC categories')
    parser.add_argument('-v', '--verbose', action='store_true',
                        help='show debugging information')
    parser.add_argument('-m', '--month', type=int,
                        help='The month (1-12')
    parser.add_argument('-y', '--year', type=int,
                        help='The year (YYYY)')
    parser.add_argument('-S', '--skip-adding', action='store_true',
                        help='Skip adding pages to the category')
    parser.add_argument('-R', '--skip-removing', action='store_true',
                        help='Skip removing pages from the category')
    parser.add_argument('-n', '--dry-run', action='store_true',
                        help='Dry run')
    parser.add_argument('-f', '--force', action='store_true',
                        help='Do not ask permission to write')
    parser.add_argument('-b', '--bot-flag', action='store_true',
                        help='Write pages with a bot flag')
    args = parser.parse_args()

    log_level = logging.DEBUG if args.verbose else logging.INFO
    logging.basicConfig(level=log_level)

    logging.getLogger("urllib3").setLevel(logging.WARNING)
    logging.getLogger("oauthlib").setLevel(logging.WARNING)
    logging.getLogger("requests").setLevel(logging.WARNING)
    logging.getLogger("requests_oauthlib").setLevel(logging.WARNING)


    if args.month and (args.month < 1 or args.month > 12):
        raise ValueError("Invalid month: {}".format(args.month))

    dotenv.load_dotenv()

    site = pywikibot.Site('en', 'wikisource')
    statn = statistician.Statistician(site)

    target_category = statn.get_category(args.year, args.month)

    logging.info(f"Target category name: {target_category.title()}")

    indexes = statn.get_indexes(args.year, args.month)

    # add cats to indexes in the MC
    if not args.skip_adding:
        for index in indexes:

            if target_category in index.categories():
                logging.debug(f"{index.title()} already in target category")
                continue

            logging.info(f"{index.title()} not in target category")

            old_text = index.text
            new_text = pywikibot.textlib.replaceCategoryLinks(
                    old_text, [target_category], add_only=True, site=site)

            index.text = new_text

            if confirm_write(args.dry_run, args.force):
                index.save(summary=f"Adding to Monthly Challenge category: [[:{target_category.title()}]]",
                    botflag=args.bot_flag)

    # remove cats from indexes not in the MC
    if not args.skip_removing:
        for page in target_category.members():

            if page.namespace().canonical_name != "Index":
                logging.debug(f"Page {page.title()} not in the Index namespace")
                continue

            logging.debug(f"Checking whether {page.title()} is in the MC")

            if page not in indexes:
                logging.info(f"{page.title()} is not in the MC")

                # remove the category
                old_text = page.text
                new_text = pywikibot.textlib.replaceCategoryInPlace(
                        old_text, target_category, None, add_only=False,
                        site=site)

                page.text = new_text

                if confirm_write(args.dry_run, args.force):
                    page.save(summary=f"Removing from Monthly Challenge category: [[:{target_category.title()}]]",
                        botflag=args.bot_flag)

if __name__ == "__main__":
    main()