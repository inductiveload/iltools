#! /usr/bin/env bash
#
## Update the Monthly Challenge statistics
#
# update-db: update the tables for the current month's category
# write-stats: generate and write statistics, categories and missing pages to the wiki

set -ex

m=$(LANG=en_us_88591; date "+%B")
y=$(date "+%Y")
todayD=$(date "+%d")
mnum=$(date "+%m")

# The date after which we start doing the next month's stats as well
# as well as pre-generating the next month's pages if missing
doNextMonthFrom=23

nextMonthM=$(date --date=next-month "+%B")
nextMonthMNum=$(date --date=next-month "+%m")
nextMonthY=$(date --date=next-month "+%Y")

WS_STAT_TOOL_DIR="$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"

# source the virtualenv
if [[ $WS_STAT_VENV ]]
then
    source "${WS_STAT_VENV}/bin/activate"
fi

python --version

function update_month_db {
    echo "Running database update script for: $1 $2"

    python $WS_STAT_TOOL_DIR/update_db.py \
        --year "$1" --month "$2"
}

cmd=$1

case $cmd in
"update-db")
    update_month_db $y $mnum

    # run updates for next months works
    if [[ "$todayD" -gt ${doNextMonthFrom} ]]
    then
        update_month_db $nextMonthY $nextMonthMNum
    fi
    ;;
"write-stats")
    echo "Running wiki stats update for ${y}-${mnum}"

    python $WS_STAT_TOOL_DIR/gen_stats.py \
        --month "$mnum" --year "$y" \
        --force --bot-flag --total-stats --daily-stats

    echo "Running categorisation for ${y}-${mnum}"
    python $WS_STAT_TOOL_DIR/categorise.py \
        --month "$mnum" --year "$y" \
        --force --bot-flag

    # in the last week of the month, create missing pages
    # and start cats for next month
    if [[ "$todayD" -gt ${doNextMonthFrom} ]]
    then
        echo "Running month rollover for ${nextMonthY}-${nextMonthMNum}"

        python $WS_STAT_TOOL_DIR/month_start.py \
            --month "$nextMonthMNum" --year "$nextMonthY" \
            --force --bot-flag

        # generate the NEXT months daily stats (will be empty daily tallies
        # and will also update the totals)
        python $WS_STAT_TOOL_DIR/gen_stats.py \
            --month "$nextMonthMNum" --year "$nextMonthY" \
            --day 0 \
            --force --bot-flag --total-stats --daily-stats

        echo "Running categorisation for ${nextMonthY}-${nextMonthMNum}"
        python $WS_STAT_TOOL_DIR/categorise.py \
            --month "$nextMonthMNum" --year "$nextMonthY" \
            --force --bot-flag
    fi
    ;;
esac
