#! /usr/bin/env python

import argparse
import logging
import dotenv
import luadata

import pywikibot

# our common code
import statistician

class EnWsBlankPages():

    def __init__(self, site, year, month) -> None:
        self.site = site
        self.year = year
        self.month = month

    def get_category_content(self):
        return f"{{{{Monthly Challenge month cat|{self.year}|{self.month:02d}}}}}"

    def get_data_content(self):
        target = 2000
        months = 3

        data = {
            'target': target,
            'year': self.year,
            'month': self.month,
            'works': {
            }
        }

        for i in range(months):
            data['works'][i] = []

        return "return " + luadata.serialize(data, indent='\t')

    def get_project_page_content(self):
        return f"{{{{Monthly Challenge month overview|{self.year}|{self.month:02d}}}}}"

def main():

    parser = argparse.ArgumentParser(description='Set up minimal pages at MC start')
    parser.add_argument('-v', '--verbose', action='store_true',
                        help='show debugging information')
    parser.add_argument('-y', '--year', type=int,
                        help='The year (YYYY)')
    parser.add_argument('-m', '--month', type=int,
                        help='The month (1-12) to set up')
    parser.add_argument('-n', '--dry-run', action='store_true',
                        help='Dry run')
    parser.add_argument('-f', '--force', action='store_true',
                        help='Do not ask permission to write')
    parser.add_argument('-b', '--bot-flag', action='store_true',
                        help='Write pages with a bot flag')
    args = parser.parse_args()

    log_level = logging.DEBUG if args.verbose else logging.INFO

    logging.basicConfig(level=log_level)

    logging.getLogger("urllib3").setLevel(logging.WARNING)
    logging.getLogger("oauthlib").setLevel(logging.WARNING)
    logging.getLogger("requests").setLevel(logging.WARNING)
    logging.getLogger("requests_oauthlib").setLevel(logging.WARNING)

    site = pywikibot.Site('en', 'wikisource')
    statn = statistician.Statistician(site)

    blank_pages = EnWsBlankPages(site, args.year, args.month)

    target_category = statn.get_category(args.year, args.month)

    if target_category.exists():
        logging.info(f"Category already exists: {target_category.title()}")
    else:
        logging.info(f"Category does not exist: {target_category.title()}")

        content = blank_pages.get_category_content()

        logging.info(f"Creating category with content:\n{content}")

        category = pywikibot.Category(site, target_category.title())
        category.text = content

        if statistician.confirm_write(args.dry_run, args.force):
            category.save(summary="Creating category", botflag=args.bot_flag)

    target_data_module = statn.get_data_module(args.year, args.month)

    if target_data_module.exists():
        logging.info(f"Data module already exists: {target_data_module.title()}")
    else:
        logging.info(f"Data module does not exist: {target_data_module.title()}")

        content = blank_pages.get_data_content()

        logging.info(f"Creating data module with content:\n{content}")

        data_module = pywikibot.Page(site, target_data_module.title())
        data_module.text = content

        if statistician.confirm_write(args.dry_run, args.force):
            data_module.save(summary="Creating data module", botflag=args.bot_flag)

    target_project_page = statn.get_project_page(args.year, args.month)
    if target_project_page.exists():
        logging.info(f"Project page already exists: {target_project_page.title()}")
    else:
        logging.info(f"Project page does not exist: {target_project_page.title()}")

        content = blank_pages.get_project_page_content()

        logging.info(f"Creating project page with content:\n{content}")

        project_page = pywikibot.Page(site, target_project_page.title())
        project_page.text = content

        if statistician.confirm_write(args.dry_run, args.force):
            project_page.save(summary="Creating monthly project page", botflag=args.bot_flag)

if __name__ == "__main__":
    main()