#! /usr/bin/env python3

import argparse
import logging
import os
import dotenv
import re

import pywikibot
import pywikibot.proofreadpage

import statistician

import mysql.connector

from contextlib import closing


def sql_all_revs_for_index_pages_since(conn, index_id, since):
    """
    Returns SQL to get the latest revision of every page in an index since a
    certain date
    """

    if not since:
        since = 19700101000000

    logging.info(f"Getting revisions on index {index_id} since {since}")

    cursor = conn.cursor(dictionary=True)

    sql = """
SELECT p.page_title, p.page_id, r.rev_id, r.rev_timestamp, tl.tl_from as index_id
FROM page AS p
INNER JOIN revision as r on r.rev_page = p.page_id
RIGHT JOIN linktarget as lt on p.page_title = lt.lt_title
JOIN templatelinks as tl on tl.tl_target_id = lt.lt_id
WHERE p.page_namespace=%s
    AND tl.tl_from=%s
    AND r.rev_timestamp > %s
ORDER BY p.page_id
;"""

    cursor.execute(sql, (104, index_id, since))
    logging.debug(cursor.statement)
    revs =  cursor.fetchall()

    logging.info(f"Got {len(revs)} revisions")

    return revs


def connect_db(host, db):

    user = os.getenv('MYSQL_USER')
    password = os.getenv('MYSQL_PASSWORD')

    # toolforge user database
    if db.startswith('__'):
        db = user + db
        host = os.getenv('MYSQL_TOOLSDB_HOST')
        port = os.getenv('MYSQL_TOOLSDB_PORT')
    else:
        port = os.getenv('MYSQL_MW_PORT')
        host = os.getenv('MYSQL_MW_HOST')

    conn = mysql.connector.connect(
        user=user,
        host=host,
        password=password,
        port=port,
        database=db
    )

    return conn


class IndexHistorian():

    def __init__(self, site):

        self.site = site
        self.batch_size = 25

    def _create_ih_tables(self, db):
        cursor = self.ih_conn.cursor()

        try:
            sql = """
CREATE TABLE IF NOT EXISTS indexes (
    index_id INT PRIMARY KEY,
    index_title varbinary(255) NOT NULL,
    index_updated varbinary(14) NOT NULL
);"""

            cursor.execute(sql)

            sql = """
CREATE TABLE IF NOT EXISTS pages (
    page_id INT PRIMARY KEY,
    page_index_id INT NOT NULL,
    page_title varbinary(255) NOT NULL,
    FOREIGN KEY (page_index_id) REFERENCES indexes(index_id)
);"""

            cursor.execute(sql)

            sql = """
CREATE TABLE IF NOT EXISTS pr_history (
    prh_rev_id INT PRIMARY KEY,
    prh_page_id INT NOT NULL,
    prh_rev_time varbinary(14) NOT NULL,
    prh_status INT NOT NULL,
    prh_user varbinary(255) NOT NULL,
    FOREIGN KEY (prh_page_id) REFERENCES pages(page_id)
);"""

            cursor.execute(sql)

            sql = """
CREATE INDEX IF NOT EXISTS pr_history_index
ON pr_history (prh_rev_id, prh_rev_time, prh_status)
;"""
            cursor.execute(sql)

            sql = """
CREATE INDEX IF NOT EXISTS pages_index
ON pages (page_id, page_index_id)
;"""
            cursor.execute(sql)

            sql = """
CREATE INDEX IF NOT EXISTS indexes_index
ON indexes (index_id, index_updated)
;"""
            cursor.execute(sql)

        except mysql.connector.Error as err:

            print("Failed creating tables: {}".format(err))
            raise

    def init_dbs(self, revs_db):

        self.ih_conn = connect_db(
            'tools.db.svc.eqiad.wmflabs',
            revs_db)

        self._create_ih_tables(self.ih_conn)

        self.mw_conn = connect_db(
            'enwikisource.analytics.db.svc.wikimedia.cloud',
            'enwikisource_p')

        print(self.ih_conn)
        print(self.mw_conn)

    def rev_in_cache(self, cursor, revid):

        cursor.execute("SELECT prh_rev_id from pr_history where prh_rev_id=%s", (revid,))
        return len(cursor.fetchall()) > 0

    def cache_page(self, page, index_id):

        cursor = self.ih_conn.cursor()

        sql = """
INSERT INTO pages (page_id, page_title, page_index_id)
VALUES (%s, %s, %s)
ON DUPLICATE KEY UPDATE
    page_title = VALUES(page_title),
    page_index_id = VALUES(page_index_id)
;"""

        cursor.execute(sql, (page.pageid, page.title(), index_id))

        cursor.close()

    def cache_page_revs(self, vals):
        cursor = self.ih_conn.cursor()
        # now, insert into the cache table
        sql = """
INSERT INTO pr_history (prh_rev_id, prh_page_id, prh_rev_time, prh_status, prh_user)
VALUES (%s, %s, %s, %s, %s)
ON DUPLICATE KEY UPDATE
    prh_page_id = VALUES(prh_page_id),
    prh_rev_time = VALUES(prh_rev_time),
    prh_status = VALUES(prh_status),
    prh_user = VALUES(prh_user)
;"""
        cursor.executemany(sql, vals)

        self.ih_conn.commit()

    def get_last_index_update(self, index_id):
        cursor = self.ih_conn.cursor()
        cursor.execute("SELECT index_updated from indexes where index_id=%s", (index_id,))

        res = cursor.fetchone()

        if not res:
            return None

        return bytes(res[0])

    def cache_rev_sql_results(self, sql_results):
        ih_cursor = self.ih_conn.cursor()

        total_cached = 0

        vals = []
        for x in sql_results:
            title = x['page_title'].decode("utf-8")
            revid = x['rev_id']

            # avoid pulling down text if we've see the revision
            if not self.rev_in_cache(ih_cursor, revid):

                # the revision in question
                page = pywikibot.page.Page(self.site, "Page:" + title)
                revtext = page.getOldVersion(oldid=revid)

                self.cache_page(page, x['index_id'])

                pr_info = None

                # None if revision was deleted or is otherwise missing
                if revtext is not None:
                    try:
                        pr_info = self.get_pr_info(revtext)
                    except Exception as e:
                        logging.error(f"Error parsing {page.title}:")
                        logging.exception(e)

                if pr_info is not None:
                    update_val = (revid, page.pageid,
                                bytes(x['rev_timestamp']),
                                pr_info['level'], pr_info['user'])
                    vals.append(update_val)

                    total_cached += 1

            else:
                # print("Cached: {}".format(revid))
                pass

            # commit in batches
            if len(vals) >= self.batch_size:
                self.cache_page_revs(vals)
                vals = []

        self.cache_page_revs(vals)
        ih_cursor.close()

        return total_cached

    def cache_index_revs(self, indexes, since):

        total_cached = 0

        # First init the indexes
        self._init_indexes(indexes)

        for index in indexes:

            time = self.site.server_time()

            if since is None:
                last_update = self.get_last_index_update(index.pageid)
            elif since <= 0:
                last_update = None
            else:
                last_update = since
            myresult = sql_all_revs_for_index_pages_since(
                    self.mw_conn, index.pageid, last_update)

            print("{} revs in {}".format(len(myresult), index))

            total_cached += self.cache_rev_sql_results(myresult)

            self.update_time(index, time)

        return total_cached

    def update_time(self, index, time):

        time_s = time.totimestampformat()

        vals = []
        vals.append((time_s, index.pageid))

        sql = """
UPDATE indexes
SET index_updated = %s
WHERE index_id = %s
;"""

        with closing(self.ih_conn.cursor()) as cursor:
            cursor.executemany(sql, vals)

        self.ih_conn.commit()

        print("Updated index {} at {}".format(
                index.title(), time_s))

    def _init_indexes(self, indexes):

        vals = []
        for index in indexes:
            vals.append((index.pageid, index.title(), 0))

        sql = """
INSERT INTO indexes (index_id, index_title, index_updated)
VALUES (%s, %s, %s)
ON DUPLICATE KEY UPDATE
    index_title = VALUES(index_title)
;"""

        with closing(self.ih_conn.cursor()) as cursor:
            cursor.executemany(sql, vals)


    def get_pr_info(self, txt):

        m = re.search(r'< *pagequality *level=\"([0-9])\" *user=\"(.*?)\" */ *>', txt)

        if m:
            return {
                'level': m.group(1),
                'user': m.group(2)
            }

        return None


def main():

    parser = argparse.ArgumentParser(description='')
    parser.add_argument('-v', '--verbose', action='store_true',
                        help='show debugging information')
    parser.add_argument('-c', '--categories', nargs="+",
                        help='Categories to scan pages of')
    parser.add_argument('-I', '--indexes', nargs="+", type=str,
                        help='Indexes to scan pages of')
    parser.add_argument('-y', '--year', type=int,
                        help='MC year to update for')
    parser.add_argument('-m', '--month', type=int,
                        help='MC month to update for')
    parser.add_argument('-d', '--database', default="__idx_hist",
                        help='The database name (default: __idx_hist)')
    parser.add_argument('-S', '--since', type=int,
                        help='Only update indexes that have not been updated since this timestamp (0 to for all revisions)')

    args = parser.parse_args()

    log_level = logging.DEBUG if args.verbose else logging.INFO
    logging.basicConfig(level=log_level)

    logging.getLogger("urllib3").setLevel(logging.WARNING)
    logging.getLogger("oauthlib").setLevel(logging.WARNING)
    logging.getLogger("requests").setLevel(logging.WARNING)
    logging.getLogger("requests_oauthlib").setLevel(logging.WARNING)

    pwbl = logging.getLogger("pywiki")
    pwbl.disabled = True

    dotenv.load_dotenv()

    site = pywikibot.Site('en', 'wikisource')

    ih = IndexHistorian(site)
    ih.init_dbs(args.database)

    seen = {}
    all_indexes = []

    if args.year and args.month:
        # Do it properly and read from the MC Lua
        statn = statistician.Statistician(site)
        all_indexes += statn.get_indexes(args.year, args.month)

    if args.indexes:
        for index in args.indexes:
            index_page = pywikibot.Page(site, index, ns='Index')
            all_indexes.append(index_page)

    if args.categories:
        for c in args.categories:
            cat = pywikibot.Category(site, c)

            for index in site.categorymembers(cat, namespaces="Index"):
                # and the global list
                if index.pageid not in seen:
                    all_indexes.append(index)
                    seen[index.pageid] = True

    print("Caching index revs")
    cached = ih.cache_index_revs(all_indexes, not args.since)

    print("Cached {} revisions".format(cached))


if __name__ == "__main__":
    main()
