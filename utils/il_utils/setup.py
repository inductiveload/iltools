import setuptools

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setuptools.setup(
    name="iltools-utils",
    version="0.0.1",
    author="Inductiveload",
    author_email="inductiveload@gmail.com",
    description="Simple shared utilities",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://phabricator.wikimedia.org/source/tool-pagelister",
    project_urls={
        "Bug Tracker": "https://phabricator.wikimedia.org/project/profile/5221/",
    },
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    packages=setuptools.find_namespace_packages(include=['il_utils.*']),
    python_requires=">=3.7",
)
