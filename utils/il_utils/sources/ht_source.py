
from ..wikisource import pagelist as PL

import urllib
import json

import logging


module_logger = logging.getLogger('iltools.ht_source')


class HathiSource():

    def __init__(self, dapi, htid):
        self.htid = self._normalise_id(htid)
        self._metadata = None
        self.dapi = dapi

    def _meta(self):

        if self._metadata is None:
            r = self.dapi.getmeta(self.htid, json=True)
            self._metadata = json.loads(r)

        return self._metadata

    def get_pagelist(self):

        pl = PL.PageList()

        # print(self._meta()['htd:seqmap'])

        for seq in self._meta()['htd:seqmap'][0]['htd:seq']:

            if seq['htd:pnum']:
                pn = seq['htd:pnum']
            else:
                pn = '–'

            pl.append(pn)

        return pl

    @staticmethod
    def _normalise_id(htid):

        if "hdl.handle.net" in htid:
            return htid.split("/")[-1]
        elif "babel.hathitrust.org" in htid:
            u = urllib.parse("htid")
            u = urllib.parse_qs(u.query)
            return u['id']
        return htid

    def get_id(self):
        return self.htid
